package main

import (
	"fmt"
	"os"
	"reflect"
	"strings"

	tgbotapi "github.com/Syfaro/telegram-bot-api"

	log "github.com/sirupsen/logrus"
)

func greet(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	const greetingMessage = "Хоп Хэй! Привет%s, я Чебупель!"
	recipient := ""

	recipient = update.Message.From.FirstName
	if recipient == "" {
		recipient = update.Message.From.UserName
	}

	if recipient != "" {
		recipient = " " + recipient
	}

	msg := tgbotapi.NewMessage(update.Message.Chat.ID, fmt.Sprintf(greetingMessage, recipient))
	bot.Send(msg)
}

func pants(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	const greetingMessage = "В штанах у тебя"

	msg := tgbotapi.NewMessage(update.Message.Chat.ID, greetingMessage+update.Message.Text[6:])
	bot.Send(msg)
}

const helpCommand = "/help"
const pantsCommand = "/pants"
const startCommand = "/start"

func getAllCommands() map[string]string {
	return map[string]string{
		helpCommand:  "Помощь",
		pantsCommand: "Получить сведения о содержимом штанов",
	}
}

func help(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	commands := getAllCommands()
	strs := []string{"Доступные команды:"}
	for k := range commands {
		strs = append(strs, fmt.Sprintf("%s - %s", k, commands[k]))
	}

	msg := tgbotapi.NewMessage(update.Message.Chat.ID, strings.Join(strs, "\n"))
	bot.Send(msg)
}

func telegramBot(token string) {

	// Creating the tg bot
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		panic(err)
	}

	// Setting the update time
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	// Getting the updates from bot
	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		//Checking if user sent text
		if reflect.TypeOf(update.Message.Text).Kind() == reflect.String && update.Message.Text != "" {
			if update.Message.Text == startCommand {
				greet(bot, update)
			} else if update.Message.Text == helpCommand {
				help(bot, update)
			} else if len(update.Message.Text) > 5 && update.Message.Text[:6] == pantsCommand {
				pants(bot, update)
			} else {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Я не знаю такой команды :(")
				bot.Send(msg)
			}
		} else {
			//Отправлем сообщение
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Пиши текстом, чепушила!")
			bot.Send(msg)
		}
	}
}

func main() {
	token := os.Getenv("TOKEN")
	if token == "" {
		log.Fatal("no token")
	}

	//Вызываем бота
	telegramBot(token)
}
